const controller = require('../controller/controller');
module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.get('/', (req, res) => res.send('index'));
    app.get('/users', controller.allUsers);
    app.get('/users/name/:name', controller.findName);
    app.get('/users/:id', controller.findId);
    app.put('/users/:id', controller.update);
    app.get('/status', controller.status)
    app.post('/status/create', controller.createStatus);
    app.get('/balance', controller.balance)
    app.post('/admins/create', controller.createAdmins);
    app.get('/website', controller.allWebsite);
    app.post('/login', controller.login);
    app.get('/customer-transactions', controller.customer)
    app.get('/customer-transactions/:id', controller.customer_findOne)
    app.get('/user-transactions', controller.user_transactions)
    app.get('/user-transactions/:id', controller.user_transactions_findOne)
    app.get('/transactions', controller.transactions)
}