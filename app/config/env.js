const Sequelize = require('sequelize');
const db = 'websitedotid'
const user = 'root'
const password = ''
const host = 'localhost'
module.exports = new Sequelize(db, user, password, {
    host: host,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});