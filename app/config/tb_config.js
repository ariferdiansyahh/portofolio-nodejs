const Sequelize = require('sequelize')
const db = {}
db.Sequelize = Sequelize;
db.users = require('../model/users')
db.user_statuses = require('../model/user_statuses')
db.websites = require('../model/websites')
db.admins = require('../model/admins')
db.customer_transactions = require('../model/customer_transactions')
db.user_transactions = require('../model/user_transactions')
db.transactions = require('../model/transactions')
db.users.hasMany(db.user_statuses, {
    foreignKey: 'user_id',
    otherKey: 'id'
});
db.user_statuses.belongsTo(db.users, {
    foreignKey: 'user_id',
})
db.websites.belongsTo(db.users, {
    foreignKey: 'user_id',
});
db.user_transactions.belongsTo(db.transactions, {

    foreignKey: 'transaction_id',
    targetKey: 'id'
});
db.user_transactions.belongsTo(db.users, {
    foreignKey: 'user_id',
});
db.customer_transactions.belongsTo(db.transactions, {
    foreignKey: 'transaction_id',
    targetKey: 'id'
});

module.exports = db;