const Sequelize = require('sequelize');
const db = require('../config/env');
const customer_transactions = db.define('customer_transactions', {
    transaction_id: {
        type: Sequelize.INTEGER
    },
    first_name: {
        type: Sequelize.STRING
    },
    middle_name: {
        type: Sequelize.STRING
    },
    last_name: {
        type: Sequelize.STRING
    },
    email_address: {
        type: Sequelize.STRING
    },
    address: {
        type: Sequelize.STRING
    },
    province_id: {
        type: Sequelize.BIGINT
    },
    city_id: {
        type: Sequelize.BIGINT
    },
    postal_code: {
        type: Sequelize.BIGINT
    },
    phone_number: {
        type: Sequelize.STRING
    },
    shipping_method: {
        type: Sequelize.STRING
    },
    ordered_item_id: {
        type: Sequelize.BIGINT
    }

}, {
    underscored: true
});

module.exports = customer_transactions;