const Sequelize = require('sequelize');
const db = require('../config/env');

const transactions = db.define('transactions', {

    transaction_type:{
        type: Sequelize.STRING
    },
    invoicenumber: {
        type: Sequelize.STRING
    },
    totalamount: {
        type: Sequelize.DOUBLE
    },
    words: {
        type: Sequelize.STRING
    },
    response_code: {
        type: Sequelize.STRING
    },
    approvalcode: {
        type: Sequelize.STRING
    },
    trxstatus: {
        type: Sequelize.STRING
    },
    payment_channel: {
        type: Sequelize.INTEGER
    },
    paymentcode: {
        type: Sequelize.STRING
    },
    session_id: {
        type: Sequelize.STRING
    },
    bank_issuer: {
        type: Sequelize.STRING
    },
    payment_date_time: {
        type: Sequelize.DATE
    },
    verifyid: {
        type: Sequelize.STRING
    },
    verifyscore: {
        type: Sequelize.INTEGER
    },
    verifystatus: {
        type: Sequelize.STRING
    },
}, {
    underscored: true
});


module.exports = transactions;