const Sequelize = require('sequelize');
const db = require('../config/env');
const users = db.define('users', {
    uuid: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    }
}, {
    underscored: true
});

module.exports = users;