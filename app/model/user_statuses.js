const Sequelize = require('sequelize');
const db = require('../config/env');

const user_statuses = db.define('user_statuses', {
    user_id: {
        type: Sequelize.BIGINT
    },
    status_id: {
        type: Sequelize.BIGINT
    },
}, {
    underscored: true
});


module.exports = user_statuses;