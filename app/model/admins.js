const Sequelize = require('sequelize');
const db = require('../config/env');

const admins = db.define('admins', {
    email: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    }
}, {
    underscored: true
});


module.exports = admins;