const Sequelize = require('sequelize');
const db = require('../config/env');
const websites = db.define('websites', {
    uuid: {
        type: Sequelize.STRING
    },
    user_id: {
        type: Sequelize.BIGINT
    },
    domain_primary: {
        type: Sequelize.STRING
    },
    subdomain_default: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
    website_type: {
        type: Sequelize.BIGINT
    }
}, {
    underscored: true
});
module.exports = websites;