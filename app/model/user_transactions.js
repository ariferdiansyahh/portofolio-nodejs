const Sequelize = require('sequelize');
const db = require('../config/env');

const user_transactions = db.define('user_transactions', {
    uuid: {
        type: Sequelize.STRING
    },
    transaction_id: {
        type: Sequelize.INTEGER
    },
    user_id: {
        type: Sequelize.BIGINT
    },
    transaction_name: {
        type: Sequelize.STRING
    },
    subtotal: {
        type: Sequelize.INTEGER
    }
}, {
    underscored: true
});


module.exports = user_transactions;