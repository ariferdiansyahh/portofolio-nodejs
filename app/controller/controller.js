//REQUIRE
var express = require('express')
const config = require('../config/config')
const bcrypt = require('bcrypt')
const sequelize = require('sequelize')
const jwt = require('jsonwebtoken')
var bodyParser = require('body-parser')
const axios = require('axios')
const tb = require('../config/tb_config')
const app = express()
//LODASH
var _ = require('lodash');
// Load the core build.
var _ = require('lodash/core');
// Load the FP build for immutable auto-curried iteratee-first data-last methods.
var fp = require('lodash/fp');
// Load method categories.
var array = require('lodash/array');
var object = require('lodash/fp/object');
// Cherry-pick methods for smaller browserify/rollup/webpack bundles.
var at = require('lodash/at');
var curryN = require('lodash/fp/curryN');
//MODEL
const users = tb.users;
const websites = tb.websites;
const user_statuses = tb.user_statuses;
const admins = tb.admins;
const customer_transactions = tb.customer_transactions
const user_transactions = tb.user_transactions
const transactions = tb.transactions
//CONTROLLER
exports.allUsers = async (req, res) => {
    try {
        const data = await users.findAll({
            include: {
                model: user_statuses,
                where: {
                    user_id: {
                        $col: 'user_id'
                    }
                },
                order: [
                    ['created_at', 'DESC'],
                ],
                limit: 1
            }
        })
        const dataPagination = data
        const jumlah = await users.count()
        return res.json({
            "jumlah": jumlah,
            "data": dataPagination
        });
    } catch (err) {
        console.log(err)
    };
}
exports.allWebsite = async (req, res) => {
    try {
        const data = await websites.findAll({
            include: {
                model: users,
            }
        })
        const jumlah = await websites.count()
        const ecommerce = await websites.count({
            where: {
                $or: [{
                    website_type: 1
                }]
            },
        })
        const company_profil = await websites.count({
            where: {
                $or: [{
                    website_type: 2
                }]
            },
        })
        return res.json({
            "jumlah": jumlah,
            ecommerce,
            company_profil,
            data
        })
    } catch (err) {
        console.log(err)
    }
}
exports.createAdmins = async (req, res) => {
    try {
        var numSaltRounds = 10;
        const data = await admins.create({
            email: req.body.email,
            name: req.body.name,
            password: bcrypt.hashSync(req.body.password,numSaltRounds)
        });
        return res.json(data)
    } catch (err) {
        console.log(err)
    }
}

exports.status = async (req, res) => {
    try {
        const data = await users.findAll({
            include: {
                model: user_statuses,
                where: {
                    user_id: {
                        $col: 'user_id'
                    }
                },
                order: [
                    ['created_at', 'DESC'],
                ],
                limit: 1

            }
        })
        const obj = data.map(res => {
            return res.user_statuses[0]
        })
        const bann = _.filter(obj, {
            'status_id': 5
        }).length
        const active = _.filter(obj, {
            'status_id': 2
        }).length
        return res.json({
            obj,
            bann,
            active
        })
    } catch (err) {
        console.log(err)
    }
}

exports.createStatus = async (req, res) => {
    try {
        const data = await user_statuses.create({
            user_id: req.body.user_id,
            status_id: req.body.status_id
        });
        return res.json(data)
    } catch (err) {
        console.log(err)
    }
}
exports.findId = async (req, res, next) => {
    try {
        const data = await users.findOne({
            returning: true,
            where: {
                id: req.params.id
            }
        });
        return res.json({

            data
        })
    } catch (err) {
        console.log(err)
    };
}
exports.findName = async (req, res, next) => {
    try {
        const data = await users.findOne({
            returning: true,
            where: {
                name: {
                    $like: req.params.name
                }
            }
        });
        return res.json({

            data
        })
    } catch (err) {
        console.log(err)
    };
}

exports.update = async (req, res, next) => {
    try {
        const data = await users.update({
            name: req.body.name,
            email: req.body.email,
        }, {
            returning: true,
            where: {
                id: req.params.id
            }
        });
        return res.json({
            "status": "updated",
            data
        })
    } catch (err) {
        console.log(err)
    };
}

exports.login = async (req, res) => {
    console.log("Sign-In");
    admins.findOne({
        where: {
            name: req.body.name
        }
    }).then(name => {
        if (!name) {
            return res.status(404).send('User Not Found.');
        }
        var passwordIsValid = bcrypt.compareSync(req.body.password, name.password);
        if (!passwordIsValid) {
            return res.status(401).send({
                auth: false,
                accessToken: null,
                reason: "Invalid Password!"
            });
        }
        var token = jwt.sign({
            id: name.id
        }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        res.status(200).json({
            auth: true,
            accessToken: token
        });
    }).catch(err => {
        res.status(500).send('Error -> ' + err);
    });
}

exports.balance = async (req, res) => {
    var params = new URLSearchParams();
    var url = 'https://mydomain.cloud.id/domainkuAPI/dapi.php';
    params.append('authemail', 'id.website@website.id');
    params.append('token', 'UHNQqCgTF5ySYXdhj6L097Wr');
    params.append('action', 'GetCreditBalance');
    params.append('sld', 'a');
    params.append('tld', 'a');
    try {
        const balance = await axios.post(url, params);
        return res.json(balance.data)
    } catch (err) {
        console.log(err)
    }
}

exports.customer = async (req, res) => {
    try {
        var customer = await customer_transactions.findAll({
            include: {
                model: transactions,
            },
        })
        return res.json({
            'data': customer
        })
    } catch (err) {
        console.log(err)
    }
}

exports.customer_findOne = async (req, res) => {
    try {
        var customer = await customer_transactions.findOne({
            include: {
                model: transactions,
            },
            where: {
                id: req.params.id
            }
        })
        return res.json({
            'data': customer
        })
    } catch (err) {
        console.log(err)
    }
}

exports.user_transactions = async (req, res) => {
    try {
        var user = await user_transactions.findAll({
            include: [{
                    model: users,
                },
                {
                    model: transactions,
                }
            ]
        })
        return res.json({
            'data': user
        })
    } catch (err) {
        console.log(err)
    }
}

exports.user_transactions_findOne = async (req, res) => {
    try {
        var user = await user_transactions.findOne({
            include: [{
                    model: users,
                },
                {
                    model: transactions,
                }
            ],
            where: {
                id: req.params.id
            }
        })
        return res.json({
            'data': user
        })
    } catch (err) {
        console.log(err)
    }
}

exports.transactions = async (req, res) => {
    try {
        var user = await transactions.findAll({})
        return res.json({
            'data': user
        })
    } catch (err) {
        console.log(err)
    }
}