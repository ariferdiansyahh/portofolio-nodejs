const express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

//DATABASE
const db = require('./app/config/env')
//DB TEST
db.authenticate()
    .then(() => console.log('Databasee connected...'))
    .catch(err => console.log("error:" + err))
//ROUTER
require('./app/router/route')(app);

const port = process.env.port || 8000;
app.listen(port, console.log(`server start on port ${port}`));